#!/usr/bin/env bash

prince examples/plos-one/index.html --style=themes/plos-one/print.css --output examples/plos-one/plos-one.pdf
prince examples/plos-one/index.html --style=themes/nature/print.css --output examples/plos-one/nature.pdf